math.randomseed(os.time())
for i = 1, 3 do -- Discard first few math.random()
	math.random()
end
-- A reverse ipairs. Taken from http://lua-users.org/wiki/IteratorsTutorial
function ripairs(t)

  local max = 1
  
  while t[max] ~= nil do
   
    max = max + 1
  
  end

  local function ripairs_it(t, i)
   
    i = i-1
    local v = t[i]
    
    if v ~= nil then
      
      return i,v
    
    else
      
      return nil
   
    end 
  end
  
  return ripairs_it, t, max

end

--Variables

local width = 100 -- Number of cells in width
local height = 100 -- Number of cells in height
local border = 2 -- Border Thickness
local wWidth = 600 -- Window Width
local wHeight = 600 -- Window Height
local bWidth = wWidth/width -- Cell width
local bHeight = wHeight/height -- Cell Height
local kill_no = 6 -- Number of empty cells needed for a miner to die
local spawn_prob = 0.6 --Probablitity of a miner spawning a clone
local cell_kill_no = 6 -- Number of empty cells needed for a cell to die
local miners_no = 3 -- Number of initial miners
local fill = true -- Fill or not
local fill_no = 2 -- Treshold for filling cells
local debug = true -- Debug mode, prints info to console

map = {}
miners = {}

local count = 0
local clean = false
local printed_count = false

-- Generate map

for x = 1, width do
	
  map[x] = {}
	
  for y = 1, height do
		
    map[x][y] = true
	
  end
end

--Generate initial miners

for i = 1, miners_no do
	
  table.insert(miners,{x = math.random(1,width), y = math.random(1,height)})
  
  if debug then
    
    print("[Init-Spawn]> x= "..miners[i].x..", y= "..miners[i].y)
  
  end
end

function love.load()
  
  love.window.setTitle("Procedural Generation v2.0")
  love.window.setMode(wWidth, wHeight, {resizable = false})

end

function love.draw()

  for x = 1, #map do

    for y = 1, #map[x] do

      if map[x][y] then

        love.graphics.setColor(255,255,255)
        love.graphics.rectangle("fill", x*bWidth-bWidth, y*bHeight-bHeight, bWidth, bHeight)

      end
   end
  end

  if (#miners > 0) then

   for i , miner in ripairs(miners) do

      local empty = 0

      for x = -1, 1 do

        for y = -1 , 1 do

          if (not map[miner.x+x][miner.y+y]) or (miner.x+x < border or miner.x+x > width-border) or (miner.y+y < border or miner.y+y > height-border) then

            empty = empty +1

          end
        end
      end

      if empty >= kill_no then

        love.graphics.setColor(120,120,120)
        love.graphics.rectangle("fill", miner.x*bWidth-bWidth, miner.y*bHeight-bHeight, bWidth, bHeight)

        if debug then

          print("[Kill]> xPos = "..miner.x..", yPos = "..miner.y..", empty = "..empty)

        end 

        table.remove(miners,i)

      else

        if math.random() <= spawn_prob then

          table.insert(miners, {
            x = miner.x,
            y = miner.y
          })

          if debug then

            print("[Spawn]> x= "..miner.x..", y= "..miner.y)

          end
        end
      end

      --Move the miner

      local xDir, yDir = math.random(-1,1), math.random(-1,1)

      if (miner.x+xDir >= border and miner.x+xDir <= width-border) and (miner.y+yDir >= border and miner.y+yDir <= height-border) then

      miner.x = miner.x+xDir
      miner.y = miner.y+yDir
      love.graphics.setColor(255,0,0)
      love.graphics.rectangle("fill", miner.x*bWidth-bWidth, miner.y*bHeight-bHeight, bWidth, bHeight)

      end

      if map[miner.x][miner.y] then

        count = count + 1

        if debug then

          print("[Mine]> c = "..count..", xPos = "..miner.x..", yPos = "..miner.y)

        end

        map[miner.x][miner.y] = false

      end
    end
  end

  if #miners == 0 then

    if not clean then 

      for i = 1, 5 do

        for x = 2 , #map-1 do
   
          for y = 2, #map[x]-1 do
   
            local empty = 0
      
            for dX = -1,1 do          
            
              for dY = -1 ,1 do
            
                if not map[x+dX][y+dY] then
            
                  empty = empty + 1
            
                end
              end
            end
      
            if empty >= cell_kill_no then

              map[x][y] = false
      
            end
      
            if fill then 
      
              if (empty <= fill_no) and (not map[x][y]) then
      
                map[x][y] = true

              end
            end 
          end        
        end  
      end

      clean = true

    end

    for x = 1, #map do
    
      for y = 1, #map[x] do
    
        if map[x][y] then
    
          love.graphics.setColor(255,255,255)
    
        else
    
          love.graphics.setColor(0,0,0)
    
        end
    
        love.graphics.rectangle("fill", x*bWidth-bWidth, y*bHeight-bHeight, bWidth, bHeight)
    
      end
    end
  end

  if clean and not printed_count then

    print("Mined cells: "..count)
    printed_count = true

  end
end