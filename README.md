#Procedural Cave Generation v2

###Info

Written in Lua

Graphics printed with [Lua LOVE](https://love2d.org/)

.gif of the code in action: https://gyazo.com/1e7e5507b4c60171644a78ab61f2b458

Generation learned/"stolen" from [/u/jellysnake](https://www.reddit.com/r/gamedev/comments/3y9qqh/procedural_cave_generation_tutorial/)